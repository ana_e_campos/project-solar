<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index(){
        $contacts = Contact::all();
        return view('site.index', ['contacts' => $contacts]);
    }

    public function listContacts(){
        $contacts = Contact::all();
        return view('admin.contacts.list', ['contacts' => $contacts]);
    }


    public function create(){
        $contacts = Contact::all();
        return view('admin.contacts.create-edit', ['contacts' => $contacts]);
    }

    public function store(Request $request){
        $contact = new contact;
        $contact->nome = $request->nome;
        $contact->email = $request->email;
        $contact->telefone = $request->telefone;
        $contact->valor = $request->valor;
        $contact->arquivo = $request->arquivo;
        $contact->status = $request->status;

         //arquivo upload
         if($request->hasFile('arquivo') && $request->file('arquivo')->isValid()){

            $requestArquivo = $request->arquivo;

            $extension = $requestArquivo->extension();

                $arquivoName = md5($requestArquivo->getClientOriginalName() . strtotime("now")) . "." . $extension;

                $request->arquivo->move(public_path('img/contas-contacts/'.$contact->nome), $arquivoName);

                $contact->arquivo = $arquivoName;

        }

        $contact->save();

        return redirect('/obrigado')->with('msg', 'Olá, '.$contact->nome);


    }

    public function destroy($id){

        Contact::findOrFail($id)->delete();

        return redirect('/admin/contacts/list')->with('msg', 'Contato excluído com sucesso');

    }

    public function edit($id){

        $contact = Contact::findOrFail($id);

        return view('admin.contacts.create-edit', ['contact' => $contact]);

    }

    public function update(Request $request){
        if (isset($request->nome)){
            $data['nome'] = $request->nome;
        }
        if (isset($request->email)){
            $data['email'] = $request->email;
        }
        if (isset($request->telefone)){
            $data['telefone'] = $request->telefone;
        }
        if (isset($request->valor)){
            $data['valor'] = $request->valor;
        }
        if (isset($request->status)){
            $data['status'] = $request->status;
        }

         //arquivo upload
         if($request->hasFile('arquivo') && $request->file('arquivo')->isValid()){

            $requestArquivo = $request->arquivo;

            $extension = $requestArquivo->extension();

            $arquivoName = md5($requestArquivo->getClientOriginalName() . strtotime("now")) . "." . $extension;

            $request->arquivo->move(public_path('img/contas-contacts/'.$data['nome']), $arquivoName);

            $data['arquivo'] = $arquivoName;

        }


        Contact::findOrFail($request->id)->update($data);

        return redirect('/admin/contacts/list')->with('msg', 'Contato editado com sucesso');



    }
}
