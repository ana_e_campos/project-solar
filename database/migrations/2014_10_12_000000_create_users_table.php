<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->foreignId('current_team_id')->nullable();
            $table->text('profile_photo_path')->nullable();
            $table->timestamps();
        });

        DB::table('users')->insert(
            [
                array ('name' => 'Administrador Softeo', 'email' => 'falecom@softeo.com.br', 'password' => '$2y$10$KcGkodBDL25b.PQ8PplmHeWCpkHBFqzq3E5xvH937.6yyWnzxz1dO','created_at' => '2021-03-22 10:30:00', 'updated_at' => '2021-03-22 11:30:00')

            ]
         );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
