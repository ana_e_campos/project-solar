@extends('layouts.main')

@section('title', 'Home')

@section('content')

<div id="formulario"></div>

<div class="row">
    <div class="col-md-4 px-5 mt-5 align-self-center">
        <h2 class="titulo-home">Faça seu orçamento sem compromisso</h2>
    </div>
    <div class="col-md-8 px-5 mt-5 align-self-center">
        <form action="/site/index" method="POST" enctype="multipart/form-data" class="mt-5 contact-site">
            @csrf
            <div class="card text-dark ">

                <div class="card-body">
                    <div class="row mt-3 g-2">
                        <p class="text-center">Insira os dados para enviar a sua solicitação de orçamento :)</p>
                        <div class="form-floating col-md-6">
                            <input type="hidden" id="status" name="status" value="0">
                            <input type="text" class="form-control" placeholder=" " id="nome" name="nome" maxlength="200" required>
                            <label for="nome"> Nome</label>
                        </div>

                        <div class="form-floating col-md-6">
                            <input type="email" class="form-control" placeholder=" " id="email" name="email" maxlength="200" required>
                            <label for="link"> E-mail</label>
                        </div>

                        <div class="form-floating col-md-6">
                            <input type="text" class="form-control" placeholder=" " id="telefone" name="telefone" required onkeypress="mascaraInteiro()" onblur="mascaraTelefone(this)" value="{{ old('telefone') }}" autocomplete="off" maxlength="10">
                            <label for="telefone"> Telefone</label>
                        </div>

                        <div class="form-floating col-md-6">
                            <input type="text" class="form-control" placeholder=" " id="valor" name="valor" required onkeyup="formatarMoeda()">
                            <label for="valor"> Valor da sua conta de energia</label>
                        </div>

                        <div class="row mt-5">
                            <div class="col-md-12 d-flex justify-content-center">
                                <label for="arquivo" class="upload"><img src="{{ asset('img/icons/file.svg') }}" alt="Icon upload" width="30"> Clique aqui para inserir conta<strong> (imagem ou pdf)</strong></label>
                                <input type="file" class="form-control-file" id="arquivo" name="arquivo" required>
                            </div>
                        </div>

                    </div>
                </div>

                <div class=" d-flex justify-content-center pb-5">
                    <button class="btn btn-lg btn-form-home">Enviar meus dados ></button>
                </div>

             </div>

        </form>

    </div>
</div>

@endsection
