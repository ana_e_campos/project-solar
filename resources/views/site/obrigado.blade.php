@extends('layouts.main')

@section('title', 'Home')

@section('content')

    <div class="obrigado mt-5">

        <div class="container mt-5">

            @if(session('msg'))

                <h2 class="msg">{{ session('msg')}} </h2>
                <p class="text-center">A Solar agradece o seu pedido de orçamento.</p>
                <p class="text-center">Já vamos entrar em contato com você :)</p>

            @endif

        </div>

    </div>

@endsection
