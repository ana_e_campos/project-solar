@extends('layouts.main-admin')

@section('title', 'Lista de contatos')

@section('content')

@section('titulopagina', 'Lista de contatos')

<div class="card text-dark bg-light">

    <div class="card-header">
        <div class="row">
            <div class="col">
                <strong> Lista de contatos</strong>
            </div>
        </div>
    </div>

    <div class="card-body">
        <table class="table table-bordered" id="tabela_contatos">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th>Nome</th>
                    <th>E-mail</th>
                    <th scope="col">Telefone</th>
                    <th scope="col">Valor da conta</th>
                    <th scope="col">Arquivo</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($contacts as $contact)
                    @if ($contact->status == 1)
                    <tr class="table-success">

                    @elseif ($contact->status == 0)
                    <tr>

                    @endif
                        <th scope="row">{{ $contact->id }}</th>
                        <td>{{ $contact->nome }}</td>
                        <td>{{ $contact->email }}</td>
                        <td>{{ $contact->telefone }}</td>
                        <td>{{ $contact->valor }}</td>
                        <td>
                            @if ($contact->status == 0)
                                Pendente
                            @else
                                Respondido
                            @endif
                        </td>
                        <td><a href="/img/contas-contacts/{{$contact->nome}}/{{ $contact->arquivo}}" download title="Fazer download"><img src="{{ asset('img/icons/download.svg') }}" alt="ícone download" width="25"></a></td>
                        <td>
                            <a href="/admin/contacts/edit/{{ $contact->id }}" class="btn btn-info" title="editar"><img src="{{ asset('img/icons/edit.svg') }}" alt="ícone editar" width="25"></a>
                        </td>
                        <td>
                            <form action="/admin/contacts/list/{{ $contact->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger" href="" title="Excluir"><img src="{{ asset('img/icons/delete.svg') }}" alt="ícone excluir" width="25"></button>
                            </form>
                        </td>
                        <th>
                            @if ( $contact->status == 0 )
                            <form action="/admin/contacts/update/{{ $contact->id }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <input type="hidden" name="status" id="status" value="1">
                                <button class="btn btn-success" title="marcar como respondido">Respondido</button>
                            </form>
                            @endif
                        </th>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<script>
    $(document).ready( function () {
      $('#tabela_contatos').dataTable({
        "order": [[ 0, 'desc' ]],
        "language": {
            "lengthMenu": "Mostrar _MENU_ por página",
            "zeroRecords": "Nenhum resultado encontrado",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtered from _MAX_ total records)",
            "search": "Pesquisar",
            "paginate": {
                "next": "Próximo",
                "previous": "Anterior",
                "first": "Primeiro",
                "last": "Último"
            },
        }
      });
 } );
 </script>

@endsection
