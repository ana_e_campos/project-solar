@extends('layouts.main-admin')

@section('title', 'Editar contato')

@section('content')

@section('titulopagina', 'Gestão de contatos')


        <form action="/admin/contacts/update/{{ $contact->id }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        <div class="card text-dark bg-light">
            <div class="card-header">
                <strong> Dados do contato</strong>
            </div>
            <div class="card-body">

                <div class="row mt-3 g-2">
                    <div class="form-floating col-md-4">
                        <input type="text" class="form-control" id="nome" name="nome" value="{{ $contact->nome ?? '' }}" placeholder="" required maxlength="200">
                        <label for="nome"> Nome</label>
                    </div>
                    <div class="form-floating col-md-4">
                        <input type="email" class="form-control" id="email" name="email" value="{{ $contact->email ?? '' }}" placeholder="" required maxlength="200">
                        <label for="link"> E-mail</label>
                    </div>
                    <div class="form-floating col-md-4">
                        <input type="text" class="form-control" id="telefone" name="telefone" value="{{ $contact->telefone ?? '' }}" placeholder="" required onkeypress="mascaraInteiro()" onblur="mascaraTelefone(this)" value="{{ old('telefone') }}" autocomplete="off" maxlength="10">
                        <label for="telefone"> Telefone</label>
                    </div>
                    <div class="form-floating col-md-4">
                        <input type="text" class="form-control" id="valor" name="valor" value="{{ $contact->valor ?? '' }}" placeholder="" required maxlength="200" onkeyup="formatarMoeda()">
                        <label for="valor"> Valor</label>
                    </div>

                    <div class="col-md-12">
                        <a href="/img/contas-contacts/{{$contact->nome}}/{{ $contact->arquivo}}" download>Download arquivo</a>
                        <img src="/img/contas-contacts/{{ $contact->nome }}/{{ $contact->arquivo}}" alt="{{ $contact->nome ?? '' }}" width="150">

                        <input type="file" class="form-control-file" id="imagem" name="imagem" value="{{ $contact->arquivo ?? '' }}" {{ ($contact->arquivo ?? "") ? '': 'required'  }}>
                    </div>
                </div>

            </div>
        </div>
        <div class="card-footer text-muted">
           <button class="btn btn-lg btn-success">Alterar dados</button>
        </div>
    </div>
</form>

@endsection

