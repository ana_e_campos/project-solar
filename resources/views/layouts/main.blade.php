<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Projete o futuro com as Placas solares da Solar">
        <meta name="keywords" content="Placa solar, economia, energia, sustentabilidade">

        <title>@yield('title')</title>

        <!-- Fonts Google-->
        <link rel="preload" href="{{asset('fonts/Rubik-Light.ttf')}}">

        <!-- Styles -->
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/style.css')}}">
        <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}" />

        <!-- Javascript -->
        <script src="{{asset('js/jquery.min.js')}}"></script>
        <script async src="{{asset('js/script.js')}}"></script>
        <script async src="{{asset('js/bootstrap.min.js')}}"></script>
    </head>

    <body class="corpo-site">
        <header>

            <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top px-5">
                <div class="container-fluid">
                    <a href="/" class="navbar-brand">
                        <img src="/img/logo.jpg" alt="Logo Solar" width="150">
                    </a>
                  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                  </button>
                  <div class="collapse navbar-collapse d-flex justify-content-end" id="navbarNavDropdown">
                    <ul class="navbar-nav">

                      <li class="nav-item">
                        <a class="nav-link" href="#">Televendas: <strong>(99) 99999-9999</strong></a>
                      </li>

                    </ul>
                  </div>
                </div>
              </nav>

        </header>
        @yield('content')
        <footer class="position-absolute bottom-0 start-50 translate-middle-x">
           <p class="copy position-absolute bottom-0 start-50 translate-middle-x">Todos os direitos reservados. <a href="https://softeo.com.br/">Softeo</a> &copy; 2020 - {{ date('Y') }}</p>
        </footer>
    </body>
</html>
