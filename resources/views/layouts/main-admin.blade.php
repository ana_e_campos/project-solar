<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="keywords" content="">

        <title>@yield('title')</title>

        <!-- Fonts Google-->
        <link rel="preload" href="{{asset('fonts/Rubik-Light.ttf')}}">

        <!-- Styles -->
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/datatables.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/style.css')}}">
        <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}" />

        <!-- Javascript -->
        <script src="{{asset('js/jquery.min.js')}}"></script>
        <script async src="{{asset('js/script.js')}}"></script>
        <script async src="{{asset('js/bootstrap.min.js')}}"></script>
        <script src="{{asset('js/datatables.min.js')}}"></script>

    </head>

    <body>
        <header>

            <nav class="navbar navbar-expand-lg navbar-light" id="navbar-menu-geral">
                <div class="collapse navbar-collapse" id="navbar">
                    <a href="/admin/dashboard/list" class="navbar-brand">
                        <img src="/img/logo.jpg" alt="Logo Solar" width="200" height="63">
                    </a>
                    <button class="navbar-toggler border-0" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                        <a href="/" class="navbar-brand">
                            <img src="/img/logo.jpg" alt="Logo" width="200" height="100">
                        </a>
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse  d-flex justify-content-end" id="navbarNavDropdown">
                        <ul class="navbar-nav  d-flex">
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="/admin/contacts/list">Contatos</a>
                            </li>

                            @auth
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="true">
                                    {{ auth()->user()->name }}
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">

                                    <li>
                                        <form method="POST" name="logout" action="{{ route('logout') }}">
                                            @csrf
                                            <a href="javascript:document.logout.submit()" class="nav-link">Sair</a>
                                        </form>
                                    </li>

                                </ul>
                            </li>
                            @endauth

                        </ul>
                    </div>
                </div>
              </nav>
        </header>
        <main>
            <div class="sessao-titulo-admin">
                <h3 class="text-uppercase">@yield('titulopagina')</h3>
            </div>
            <div class="container-sm mt-5">
                @if(session('msg'))
                    <div class="alert alert-success" role="alert">
                        <p class="msg">{{ session('msg')}}</p>
                    </div>
                @elseif(session('msg-erro'))
                    <div class="alert alert-danger" role="alert">
                      <p class="msg">{{ session('msg-erro')}}</p>
                    </div>
                @endif
                @yield('content')
            </div>
        </main>
    </body>
</html>
