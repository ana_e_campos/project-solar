<?php

use App\Http\Controllers\ContactController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/register', function () {
    return view('auth.register');
})->middleware('auth');

Route::get('/admin', [ContactController::class, 'listContacts'])->middleware('auth');

Route::get('/', function () {
    return view('site.index');
});

Route::get('/obrigado', function () {
    return view('site.obrigado');
});

Route::post('/site/index', [ContactController::class, 'store']);

Route::get('/admin/contacts/create-edit', [ContactController::class, 'create'])->middleware('auth');
Route::get('/admin/contacts/list', [ContactController::class, 'listContacts'])->middleware('auth');
Route::delete('/admin/contacts/list/{id}', [ContactController::class, 'destroy'])->middleware('auth');
Route::put('/admin/contacts/update/{id}', [ContactController:: class, 'update'])->middleware('auth');
Route::get('/admin/contacts/edit/{id}', [ContactController::class, 'edit'])->middleware('auth');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
